﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShootAI : MonoBehaviour {

     public float maximumLookDistance = 20;
     public float maximumAttackDistance = 15;
 
     public float rotationDamping = 2;

//    public float fireTime = 0.05f;
//    public int pooledAmount = 20;
//    public GameObject bulletPrefab;
    
    public int botID;
    GameObject player;

    List<GameObject> bullets;

    void Awake()
    {

    }

    void Start () {
        player = GameObject.FindGameObjectWithTag("player");
    }

    // Update is called once per frame
    void FixedUpdate () {

        
        float distance = Vector3.Distance(player.transform.position, transform.position);
        if (distance <= maximumLookDistance)
        {
            LookAtTarget();
            
            if (distance <= maximumAttackDistance)
            {
                Fire();
            }
        }

    }


    void LookAtTarget()
    {
        Vector3 dir = player.transform.position - transform.position;
        dir.y = 0;
        Quaternion rotation = Quaternion.LookRotation(dir);
        transform.rotation = Quaternion.Slerp(transform.rotation, rotation, Time.deltaTime * rotationDamping);
    }

    void Fire()
    {
        for (int i = 0; i < bullets.Count; i++)
        {
            if (!bullets[i].activeInHierarchy)
            {
                bullets[i].transform.position = transform.position;
                bullets[i].transform.eulerAngles = new Vector3(transform.eulerAngles.x, transform.eulerAngles.y - 90, transform.eulerAngles.z);
                bullets[i].SetActive(true);

                break;
            }
        }
    }
}
