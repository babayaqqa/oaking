﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;
public class NavMeshMovement : MonoBehaviour
{
    public float rotationDamping = 2;
  //  [Header("Objectpoolerdan bu ayarlanan id ye gore bullet seciliyor")]
 //   public int botID;
    GameObject player;
    private float wanderRadius;
    [HideInInspector]
    GameObject arena;
    public float speed = 10;
    private Vector3 randomPos;
    private float lookTimer;
    private float lookTimerLimit;
    float firstSpeed;

    GameManager manager;
    private NavMeshAgent agent;
    [HideInInspector]
    public Transform[] points;
    [HideInInspector]
    public int destPoint = 0;
    private void Start()
    {

        agent = GetComponent<NavMeshAgent>();
      //  agent.autoBraking = false;     
        manager = GameObject.Find("GameManager").GetComponent<GameManager>();
        points = manager.enemyPoints;
        destPoint = Mathf.RoundToInt(Random.Range(0, points.Length));
        agent.SetDestination(points[destPoint].position);




        firstSpeed = speed;
        arena = GameObject.FindGameObjectWithTag("arena");
        wanderRadius = arena.transform.localScale.x / 2;
        player = GameObject.FindGameObjectWithTag("player");

        randomPos = new Vector3(Random.insideUnitCircle.x * wanderRadius, 1.8f, Random.insideUnitCircle.y * wanderRadius);
        lookTimer = 0;
        lookTimerLimit = Random.Range(2, 8);
    }

    void Patrol()
    {
      //  agent.isStopped = false;
        if (points.Length > 0)
        {          
            if (Vector3.Distance(transform.position, points[destPoint].position) < 4f)
            {
                destPoint = Mathf.RoundToInt(Random.Range(0, points.Length));
                agent.SetDestination(points[destPoint].position);
            }
        }
   /*     sampleResult = NavMesh.SamplePosition(transform.position, out hit, agent.radius, NavMesh.AllAreas);
        if (sampleResult)
        {
            transform.position = hit.position;
            agent.enabled = true;
        }
        else
        {
            //    agent.enabled = false;
            Debug.Log("navmesh agent landed outside navmesh, cannot get back in");
        }*/
        transform.position = new Vector3(transform.position.x, Mathf.Clamp(transform.position.y, -5f, 5f), transform.position.z);
    }




    [HideInInspector]
    public Vector3 moveDirection;
    private Vector3 lookDirection;
    Vector3 newPos;
    float distanceToPlayer;
    float distanceToTargetPos;
    bool collisionWithPlayer;
    Vector3 botToPlayerVector;


    void DrawCircle(Vector3 center, float radius, Color color)
    {
        Vector3 prevPos = center + new Vector3(radius, 0, 0);
        for (int i = 0; i < 30; i++)
        {
            float angle = (float)(i + 1) / 30.0f * Mathf.PI * 2.0f;
            Vector3 newPos = center + new Vector3(Mathf.Cos(angle) * radius, 0, Mathf.Sin(angle) * radius);
            Debug.DrawLine(prevPos, newPos, color);
            prevPos = newPos;
        }
    }
    NavMeshHit hit;
    bool sampleResult;
    void FixedUpdate()
    {
     //   distanceToPlayer = Vector3.Distance(player.transform.position, transform.position);
    //    distanceToTargetPos = Vector3.Distance(transform.position, randomPos);
    //    wanderRadius = arena.transform.localScale.x / 2;

   //     botToPlayerVector = player.transform.position - transform.position;
   //     botToPlayerVector = botToPlayerVector.normalized;
        //Angle between move direction and bot2player vector. If bot and players collides, we'll use this angle not go to this direction
  //      float angle = Vector3.Angle(botToPlayerVector, transform.forward);



        lookTimer += Time.deltaTime;
        if (lookTimer <= lookTimerLimit / 2)
        {
            LookAtTarget();
        }
        else if (lookTimer > lookTimerLimit / 2)
        {
            LookForward();
        }
        if(lookTimer == lookTimerLimit)
        {
            lookTimer = 0;
            lookTimerLimit = Random.Range(2, 8);
        }

        Patrol();
        
        
        if (NavMesh.FindClosestEdge(transform.position, out hit, NavMesh.AllAreas))
        {
            DrawCircle(transform.position, hit.distance, Color.red);
            Debug.DrawRay(hit.position, Vector3.up, Color.red);
            if(hit.distance < 5)
            {
                agent.enabled = false;
            }
        }
        
        /*
                   if (distanceToTargetPos < 2f)
                   {
                       randomPos = new Vector3(Random.insideUnitCircle.x * wanderRadius, 1.8f, Random.insideUnitCircle.y * wanderRadius);
                   }

                     if (!collisionWithPlayer)
                     {
                        transform.position = Vector3.MoveTowards(transform.position, randomPos, speed * Time.deltaTime);
                     }
                     else
                     {
                         this.gameObject.GetComponent<Rigidbody>().velocity = new Vector3(0, 0, 0);
                         if (!GetComponent<BotFieldOfView>().dontMoveToThatDirectionBool)
                         {
                             transform.position = Vector3.MoveTowards(transform.position, randomPos, speed * Time.deltaTime);

                         }
                     }
        */
        moveDirection = transform.forward;
        moveDirection = moveDirection.normalized;

        if (this.gameObject.transform.position.y < 0.3f)
        {
            //1 means dead
            PlayerPrefs.SetInt(this.gameObject.tag, 1);           
        }




    }


    

    void LookAtTarget()
    {
        Vector3 dir = player.transform.position - transform.position;
        dir.y = 0;
        Quaternion rotation = Quaternion.LookRotation(dir);
        transform.rotation = Quaternion.Slerp(transform.rotation, rotation, Time.deltaTime * rotationDamping);
    }
    void DontLookAtTarget()
    {
        Vector3 dir = player.transform.position - transform.position;
        dir.y = 0;
        Quaternion rotation = Quaternion.LookRotation(dir);
        transform.rotation = Quaternion.Slerp(transform.rotation, rotation, -1 * Time.deltaTime * rotationDamping);
    }
    void LookForward()
    {
        lookDirection = randomPos - transform.position;
        lookDirection.y = 0;
        transform.rotation = Quaternion.Lerp(transform.rotation, Quaternion.LookRotation(lookDirection), 20 * Time.deltaTime);
    }


    private void OnCollisionStay(Collision collision)
    {
        if (collision.gameObject.tag == "player")
        {
            collisionWithPlayer = true;
            speed = 0.1f;
        }
        if (collision.gameObject.tag == "bot0")
        {
            speed = 0.1f;
        }
    }

    private void OnCollisionExit(Collision collision)
    {
        if (collision.gameObject.tag == "player")
        {
            collisionWithPlayer = false;
            speed = firstSpeed;
        }
        if (collision.gameObject.tag == "bot0")
        {
            speed = firstSpeed;
        }
    }

}
