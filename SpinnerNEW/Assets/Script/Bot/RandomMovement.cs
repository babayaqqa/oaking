﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;
public class RandomMovement : MonoBehaviour
{
    public float rotationDamping = 2;
  //  [Header("Objectpoolerdan bu ayarlanan id ye gore bullet seciliyor")]
 //   public int botID;
    GameObject player;

    private float wanderRadius;
    [HideInInspector]
    GameObject arena;
    public float speed = 10;
    public float maxSpeed = 20f;
    private Vector3 randomPos;
    private float lookTimer;
    private float lookTimerLimit;
    float firstSpeed;

    GameManager manager;
    Transform[] points;
    private int destPoint = 0;
    Rigidbody RBody;
    private void Start()
    {
        firstSpeed = speed;
        arena = GameObject.FindGameObjectWithTag("arena");
        wanderRadius = 70 * arena.transform.localScale.x / 2;
        player = GameObject.FindGameObjectWithTag("player");

        randomPos = new Vector3(Random.insideUnitCircle.x * wanderRadius, 1.8f, Random.insideUnitCircle.y * wanderRadius);
        lookTimer = 0;
        lookTimerLimit = Random.Range(2, 8);

        RBody = GetComponent<Rigidbody>();
        emptySpaceDetected = false;
        runFromEmptySpaceTime = 0;
    }


    [HideInInspector]
    public Vector3 moveDirection;
    private Vector3 lookDirection;
    Vector3 newPos;
    float distanceToPlayer;
    float distanceToTargetPos;
    bool collisionWithPlayer;
    Vector3 botToPlayerVector;
    public LayerMask MAsk;
    RaycastHit hit;
    RaycastHit hit1;
    RaycastHit hit2;
    [HideInInspector]
    public bool emptySpaceDetected;
    [HideInInspector]
    float runFromEmptySpaceTime;
    void FixedUpdate()
    {
        distanceToPlayer = Vector3.Distance(player.transform.position, transform.position);
        distanceToTargetPos = Vector3.Distance(transform.position, randomPos);
        //  wanderRadius = arena.transform.localScale.x / 2;
        wanderRadius = 70 * arena.transform.localScale.x / 2;
         botToPlayerVector = player.transform.position - transform.position;
        botToPlayerVector = botToPlayerVector.normalized;
        //Angle between move direction and bot2player vector. If bot and players collides, we'll use this angle not go to this direction
        float angle = Vector3.Angle(botToPlayerVector, transform.forward);

        if (RBody.velocity.magnitude > maxSpeed)
        {
            RBody.velocity = RBody.velocity.normalized * maxSpeed;
        }

        /*
                    if (Physics.Raycast(transform.position + moveDirection * 6, transform.TransformDirection(Vector3.down), out hit, Mathf.Infinity, MAsk))
                    {
                        Debug.DrawRay(transform.position + moveDirection * 6, transform.TransformDirection(Vector3.down) * hit.distance, Color.red);
                        //    Debug.Log("Bot Did Hit");
                    //    emptySpaceDetected = false;
                    }
                    else
                    {
                        Debug.DrawRay(transform.position + moveDirection * 6, transform.TransformDirection(Vector3.down) * -10, Color.black);
                        //    Debug.Log("Bot Did not Hit");
                        emptySpaceDetected = true;
                    }


                    if (Physics.Raycast(transform.position + (new Vector3(moveDirection.x+30, moveDirection.y, moveDirection.z)* 6), transform.TransformDirection(Vector3.down), out hit1, Mathf.Infinity, MAsk))
                    {
                        Debug.DrawRay(transform.position + (new Vector3(moveDirection.x + 30, moveDirection.y, moveDirection.z) * 6), transform.TransformDirection(Vector3.down) * hit1.distance, Color.yellow);
                        //    Debug.Log("Bot Did Hit");
                        //    emptySpaceDetected = false;
                    }
                    else
                    {
                        Debug.DrawRay(transform.position + (new Vector3(moveDirection.x + 30, moveDirection.y, moveDirection.z) * 6), transform.TransformDirection(Vector3.down) * -10, Color.yellow);
                        //    Debug.Log("Bot Did not Hit");
                        emptySpaceDetected = true;
                    }


                    if (Physics.Raycast(transform.position + (new Vector3(moveDirection.x - 30, moveDirection.y, moveDirection.z) * 6), transform.TransformDirection(Vector3.down), out hit2, Mathf.Infinity, MAsk))
                    {
                        Debug.DrawRay(transform.position + (new Vector3(moveDirection.x - 30, moveDirection.y, moveDirection.z) * 6), transform.TransformDirection(Vector3.down) * hit2.distance, Color.green);
                        //    Debug.Log("Bot Did Hit");
                        //    emptySpaceDetected = false;
                    }
                    else
                    {
                        Debug.DrawRay(transform.position + (new Vector3(moveDirection.x - 30, moveDirection.y, moveDirection.z) * 6), transform.TransformDirection(Vector3.down) * -10, Color.green);
                        //    Debug.Log("Bot Did not Hit");
                        emptySpaceDetected = true;
                    }

        */


        if (distanceToTargetPos < 2f)
        {
            randomPos = new Vector3(Random.insideUnitCircle.x * wanderRadius, 1.8f, Random.insideUnitCircle.y * wanderRadius);
        }
    
            if (!collisionWithPlayer)
            {
                transform.position = Vector3.MoveTowards(transform.position, randomPos, speed * Time.deltaTime);
                LookForward();
                /*lookTimer += Time.deltaTime;
                if (lookTimer <= lookTimerLimit / 2)
                {
                    LookAtTarget();
                }
                else if (lookTimer > lookTimerLimit / 2)
                {
                    LookForward();
                }
                if (lookTimer == lookTimerLimit)
                {
                    lookTimer = 0;
                    lookTimerLimit = Random.Range(2, 8);
                }*/
            }
            else
            {
                //  this.gameObject.GetComponent<Rigidbody>().velocity = new Vector3(0, 0, 0);
                if (!GetComponent<BotFieldOfView>().dontMoveToThatDirectionBool)
                {
                    transform.position = Vector3.MoveTowards(transform.position, randomPos, speed * Time.deltaTime);
                }
                else
                {
                        transform.position = Vector3.MoveTowards(transform.position, randomPos, firstSpeed * Time.deltaTime);                                     
                }
            }

        








        moveDirection = transform.forward;
        moveDirection = moveDirection.normalized;


        if (this.gameObject.transform.position.y < 0.3f)
        {
            //1 means dead
            PlayerPrefs.SetInt(this.gameObject.tag, 1);           
        }

        

    }


    

    void LookAtTarget()
    {
        Vector3 dir = player.transform.position - transform.position;
        dir.y = 0;
        Quaternion rotation = Quaternion.LookRotation(dir);
        transform.rotation = Quaternion.Slerp(transform.rotation, rotation, Time.deltaTime * rotationDamping);
    }
    void DontLookAtTarget()
    {
        Vector3 dir = player.transform.position - transform.position;
        dir.y = 0;
        Quaternion rotation = Quaternion.LookRotation(dir);
        transform.rotation = Quaternion.Slerp(transform.rotation, rotation, -1 * Time.deltaTime * rotationDamping);
    }
    void LookForward()
    {
        lookDirection = randomPos - transform.position;
        lookDirection.y = 0;
        transform.rotation = Quaternion.Lerp(transform.rotation, Quaternion.LookRotation(lookDirection), 20 * Time.deltaTime);
    }

    Vector3 dirBotToBot;
    private void OnCollisionStay(Collision collision)
    {
        if (collision.gameObject.tag == "player")
        {
            collisionWithPlayer = true;
            speed = 0.1f;
            dirBotToBot = collision.transform.position - transform.position;
            dirBotToBot.y = 0;
            transform.rotation = Quaternion.Lerp(transform.rotation, Quaternion.LookRotation(dirBotToBot), 20 * Time.deltaTime);
        }
        if (collision.gameObject.tag == "bot0")
        {
            speed = 0.1f;
            dirBotToBot = collision.transform.position - transform.position;
            dirBotToBot.y = 0;
            transform.rotation = Quaternion.Lerp(transform.rotation, Quaternion.LookRotation(dirBotToBot), 20 * Time.deltaTime);
        }
    }

    private void OnCollisionExit(Collision collision)
    {
        if (collision.gameObject.tag == "player")
        {
            collisionWithPlayer = false;
            speed = firstSpeed;
        }
        if (collision.gameObject.tag == "bot0")
        {
            speed = firstSpeed;
        }
    }

}
