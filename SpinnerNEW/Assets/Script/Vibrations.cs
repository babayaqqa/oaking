﻿using UnityEngine;

namespace UnityTools
{

    /// <summary>
    /// Generic vibration helper class in order to manage easily for all devices
    /// </summary>
    public static class Vibrations
    {

        //copied from iOSHapticFeedback plugin...
        public enum HapticLevel { SelectionChange, ImpactLight, ImpactMedium, ImpactHeavy, Success, Warning, Failure, None };

        private static bool _initialized = false;
        private static bool _vibration = false;

        private static AndroidJavaClass unityPlayer;
        private static AndroidJavaObject vibrator;
        private static AndroidJavaObject currentActivity;
        private static AndroidJavaClass vibrationEffectClass;
        private static int defaultAmplitude;

        private static void Initialize()
        {
            if (Application.platform == RuntimePlatform.Android)
            {
                unityPlayer = new AndroidJavaClass("com.unity3d.player.UnityPlayer");
                currentActivity = unityPlayer.GetStatic<AndroidJavaObject>("currentActivity");
                vibrator = currentActivity.Call<AndroidJavaObject>("getSystemService", "vibrator");
                if (GetSDKInt() >= 26)
                {
                    vibrationEffectClass = new AndroidJavaClass("android.os.VibrationEffect");
                    defaultAmplitude = vibrationEffectClass.GetStatic<int>("DEFAULT_AMPLITUDE");
                }
            }
            _initialized = true;
        }

        private static void CheckAndInitialize()
        {
            if (!_initialized)
                Initialize();
        }

        /// <summary>
        /// Set vibration and return if it is status changed or not
        /// </summary>
        /// <returns>The set.</returns>
        /// <param name="state">If set to <c>true</c> state.</param>
        public static bool Set(bool state)
        {
            if (_vibration != state)
            {
                _vibration = state;
                return true;
            }
            return false;
        }

        public static void Vibrate()
        {
            CheckAndInitialize();
            if (_vibration)
            {
                Debug.LogWarning("vibration triggerred!");
                CreateOneShot(500);
            }
        }

        public static void Vibrate(long milliseconds)
        {
            CheckAndInitialize();
            if (_vibration)
            {
                Debug.LogWarning("vibration triggerred!");
                CreateOneShot(milliseconds);
            }
        }

        public static void Vibrate(long milliseconds, int amplitude)
        {
            CheckAndInitialize();
            if (_vibration)
            {
                Debug.LogWarning("vibration triggerred!");
                CreateOneShot(milliseconds, amplitude);
            }
        }

        /// <summary>
        /// Haptic vibrations with the specified level. Need iOSHapticFeedback plugin to work. 
        /// Also you need to add an iOSHapticFeedback prefab instance to scene
        /// </summary>
        /// <param name="level">Level.</param>
        public static void Haptic(HapticLevel level)
        {
            CheckAndInitialize();
            if (_vibration)
            {
                Debug.LogWarning("vibration triggerred!");

                if (iOSHapticFeedback.Instance.IsSupported())
                {
                    iOSHapticFeedback.Instance.Trigger((iOSHapticFeedback.iOSFeedbackType)level);
                }
                else
                {
                    int amplitude = 255;
                    if (HapticLevel.ImpactLight.Equals(level))
                    {
                        amplitude = 50;
                    }
                    else if (HapticLevel.ImpactMedium.Equals(level))
                    {
                        amplitude = 180;
                    }
                    CreateOneShot(50, amplitude);
                }

            }
        }

        public static void Vibrate(long[] timings, int repeat)
        {
            CheckAndInitialize();
            if (_vibration)
            {
                Debug.LogWarning("vibration triggerred!");
                CreateWaveform(timings, repeat);
            }
        }

        public static void Vibrate(long[] timings, int[] amplitudes, int repeat)
        {
            CheckAndInitialize();
            if (_vibration)
            {
                Debug.LogWarning("vibration triggerred!");
                CreateWaveform(timings, amplitudes, repeat);
            }
        }

        public static bool HasVibrator()
        {
            CheckAndInitialize();
            return vibrator.Call<bool>("hasVibrator");
        }

        public static bool HasAmplituideControl()
        {
            CheckAndInitialize();
            if (GetSDKInt() >= 26)
            {
                return vibrator.Call<bool>("hasAmplitudeControl"); //API 26+ specific
            }
            else
            {
                return false; //If older than 26 then there is no amplitude control at all
            }

        }

        public static void Cancel()
        {
            CheckAndInitialize();
            if (Application.platform == RuntimePlatform.Android)
                vibrator.Call("cancel");
        }

        //Works on API > 25
        private static void CreateOneShot(long milliseconds)
        {
            if (Application.platform == RuntimePlatform.Android)
            {
                //If Android 8.0 (API 26+) or never use the new vibrationeffects
                if (GetSDKInt() >= 26)
                {
                    CreateOneShot(milliseconds, defaultAmplitude);
                }
                else
                {
                    OldVibrate(milliseconds);
                }
            }
            //If not android do simple solution for now
            else
            {
                Handheld.Vibrate();
            }
        }

        private static void CreateOneShot(long milliseconds, int amplitude)
        {
            if (Application.platform == RuntimePlatform.Android)
            {
                //If Android 8.0 (API 26+) or never use the new vibrationeffects
                if (GetSDKInt() >= 26)
                {
                    CreateVibrationEffect("createOneShot", new object[] { milliseconds, amplitude });
                }
                else
                {
                    OldVibrate(milliseconds);
                }
            }
            //If not android do simple solution for now
            else
            {
                Handheld.Vibrate();
            }
        }

        //Works on API > 25
        private static void CreateWaveform(long[] timings, int repeat)
        {
            //Amplitude array varies between no vibration and default_vibration up to the number of timings
            if (Application.platform == RuntimePlatform.Android)
            {
                //If Android 8.0 (API 26+) or never use the new vibrationeffects
                if (GetSDKInt() >= 26)
                {
                    CreateVibrationEffect("createWaveform", new object[] { timings, repeat });
                }
                else
                {
                    OldVibrate(timings, repeat);
                }
            }
            //If not android do simple solution for now
            else
            {
                Handheld.Vibrate();
            }
        }

        private static void CreateWaveform(long[] timings, int[] amplitudes, int repeat)
        {
            if (Application.platform == RuntimePlatform.Android)
            {
                //If Android 8.0 (API 26+) or never use the new vibrationeffects
                if (GetSDKInt() >= 26)
                {
                    CreateVibrationEffect("createWaveform", new object[] { timings, amplitudes, repeat });
                }
                else
                {
                    OldVibrate(timings, repeat);
                }
            }
            //If not android do simple solution for now
            else
            {
                Handheld.Vibrate();
            }

        }

        //Handels all new vibration effects
        private static void CreateVibrationEffect(string function, params object[] args)
        {
            AndroidJavaObject vibrationEffect = vibrationEffectClass.CallStatic<AndroidJavaObject>(function, args);
            vibrator.Call("vibrate", vibrationEffect);
        }

        //Handles old vibration effects
        private static void OldVibrate(long milliseconds)
        {
            vibrator.Call("vibrate", milliseconds);
        }
        private static void OldVibrate(long[] pattern, int repeat)
        {
            vibrator.Call("vibrate", pattern, repeat);
        }

        private static int GetSDKInt()
        {
            if (Application.platform == RuntimePlatform.Android)
            {
                using (var version = new AndroidJavaClass("android.os.Build$VERSION"))
                {
                    return version.GetStatic<int>("SDK_INT");
                }
            }
            else
            {
                return -1;
            }
        }

    }

}
