﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using CnControls;

public class MoveSpinner : MonoBehaviour {

    [HideInInspector]
    public Vector3 moveDirection;
    Vector3 movement;
    public float speed = 5.0F;
    public float maxSpeed = 20f;
    float firstSpeed;
    float h = 0;
    float v = 0;
    GameManager manager;
    bool collisionWithBot = false;

    [SerializeField]
    Vector3 target;
    Vector3 dirNormalized;
    RaycastHit hit;
    public LayerMask MAsk;
    Rigidbody RBody;
    private void Start()
    {
        manager = GameObject.Find("GameManager").GetComponent<GameManager>();
        firstSpeed = speed;
        RBody = GetComponent<Rigidbody>();
    }
    
    void FixedUpdate()
    {
        if (Physics.Raycast(transform.position + moveDirection * 6, transform.TransformDirection(Vector3.down), out hit, Mathf.Infinity, MAsk))
        {
            Debug.DrawRay(transform.position + moveDirection * 6, transform.TransformDirection(Vector3.down) * hit.distance, Color.red);
          //  Debug.Log("Did Hit");
        }
        else
        {
            Debug.DrawRay(transform.position + moveDirection * 6, transform.TransformDirection(Vector3.down) * -10, Color.black);
       //     Debug.Log("Did not Hit");
        }

        if (RBody.velocity.magnitude > maxSpeed)
        {
            RBody.velocity = RBody.velocity.normalized * maxSpeed;
        }

        if (manager.playing)
           {
                   h = CnInputManager.GetAxisRaw("Hor");
                   v = CnInputManager.GetAxisRaw("Ver");
                   movement = new Vector3(h, 0, v);


                   if (!(h == 0 && v == 0))
                   {
                       transform.rotation = Quaternion.Lerp(transform.rotation, Quaternion.LookRotation(movement), 20 * Time.deltaTime);
                   }
                    //Surekli hareket
                    transform.position += moveDirection * Time.deltaTime * speed;
                    //  transform.Translate(movement * speed * Time.deltaTime, Space.World);
                    moveDirection = transform.forward;
                   moveDirection = moveDirection.normalized;

                transform.position = new Vector3(transform.position.x, Mathf.Clamp(transform.position.y,-5f,5f), transform.position.z);

           }

    /*    if (manager.playing)
          {
            if (!collisionWithBot)
            {
                if (Input.GetAxis("Fire1") > 0f)
                {
                    Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);

                    if (Physics.Raycast(ray, out hit) && !(hit.transform.CompareTag("button")))
                    {

                        target = new Vector3(hit.point.x, transform.position.y, hit.point.z);
                        moveDirection = target - transform.position;
                        moveDirection = moveDirection.normalized;

                        transform.LookAt(new Vector3(target.x, transform.position.y, target.z));
                    }
                }
                transform.position += moveDirection * Time.deltaTime * speed;
            }
            else
            {
                this.gameObject.GetComponent<Rigidbody>().velocity = new Vector3(0,0,0);
                if (!GetComponent<FieldOfView>().dontMoveToThatDirectionBool)
                {
                    if (Input.GetAxis("Fire1") > 0f)
                    {
                        Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);

                        if (Physics.Raycast(ray, out hit) && !(hit.transform.CompareTag("button")))
                        {

                            target = new Vector3(hit.point.x, transform.position.y, hit.point.z);
                            moveDirection = target - transform.position;
                            moveDirection = moveDirection.normalized;


                            transform.LookAt(new Vector3(target.x, transform.position.y, target.z));
                        }
                    }
                    transform.position += moveDirection * Time.deltaTime * speed;
                }
                else
                {
                    if (Input.GetAxis("Fire1") > 0f)
                    {
                        Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);

                        if (Physics.Raycast(ray, out hit) && !(hit.transform.CompareTag("button")))
                        {

                            target = new Vector3(hit.point.x, transform.position.y, hit.point.z);
                            moveDirection = target - transform.position;
                            moveDirection = moveDirection.normalized;

                            transform.LookAt(new Vector3(target.x, transform.position.y, target.z));
                        }
                    }
                }
            }         
          }*/
                    
        


    }



    private void OnCollisionStay(Collision collision)
    {
        if (collision.gameObject.tag == "bot0")
        {
            collisionWithBot = true;
            //set speed 0.1 when colliding not to push each other
            speed = 0.1f;
        }


    }

    private void OnCollisionExit(Collision collision)
    {
        if (collision.gameObject.tag == "bot0")
        {
            collisionWithBot = false;
            speed = firstSpeed;
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag == "obstacle")
        {
            transform.position += moveDirection * Time.deltaTime * -speed;
        }
    }


}
