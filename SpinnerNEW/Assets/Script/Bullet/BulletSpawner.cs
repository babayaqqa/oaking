﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BulletSpawner : MonoBehaviour {

 //   public FloatRange timeBetweenSpawns, scale, randomVelocity, angularVelocity;

//    public float velocity;

    public Material stuffMaterial;
    public float BulletForceBot0 = 800;
    public BotBullet[] stuffPrefabs;
    public float fireTime = 0.05f;
    float firstTime;
    float timeSinceLastSpawn;
    float currentSpawnDelay;
  //  [HideInInspector]
    public int BotID;
    //  [HideInInspector]
    public bool dead;
    float next_spawn_time;


    private void Start()
    {
        firstTime = fireTime;
        next_spawn_time = Time.time + 0.03f;
    }

    private float timestamp = 0.0f;
    private float delay = 0.001f;
    void Update()
    {
    /*    if (Time.time > next_spawn_time)
        {
            SpawnBullet();
            next_spawn_time += 0.03f;
        }*/
        SpawnBullet();       
    }

    Vector3 botPos;
    Vector3 botDirection;
    Quaternion botRotation;
    float spawnDistance = 1.5f;
    void SpawnBullet()
    {
        spawnDistance = 1.5f * transform.localScale.x;
        botPos = transform.position;
        botDirection = transform.forward;
        botRotation = transform.rotation;
        Vector3 spawnPos = botPos + botDirection * spawnDistance;
        
                //  BotBullet prefab = stuffPrefabs[Random.Range(0, stuffPrefabs.Length)];
                    BotBullet prefab = stuffPrefabs[3 * BotID];
                    BotBullet spawn = prefab.GetPooledInstance<BotBullet>();
                    //   spawn.transform.localPosition = spawnPos + botDirection * spawnDistance;
                    spawn.transform.localPosition = spawnPos;
                    spawn.transform.rotation = botRotation;
                    //  spawn.transform.localPosition = transform.position;
                    //     spawn.transform.eulerAngles = new Vector3(transform.eulerAngles.x, transform.eulerAngles.y, transform.eulerAngles.z);
               //     spawn.bulletID = BotID;

                    BotBullet prefab2 = stuffPrefabs[3 * BotID + 1];
                    BotBullet spawn2 = prefab2.GetPooledInstance<BotBullet>();
                  //  spawn2.transform.localPosition = spawnPos + transform.right * 1.3f;
                 //   spawn2.transform.rotation = botRotation;
                  //  spawn2.transform.localPosition = spawnPos + botDirection * spawnDistance;
                    spawn2.transform.localPosition = spawnPos;
                    spawn2.transform.rotation = botRotation;
                    //  spawn2.transform.localPosition = transform.position + new Vector3(1.5f, 0, 1.5f);
                    //     spawn2.transform.eulerAngles = new Vector3(transform.eulerAngles.x, transform.eulerAngles.y, transform.eulerAngles.z);
              //      spawn2.bulletID = BotID;

                    BotBullet prefab3 = stuffPrefabs[3 * BotID + 2];
                    BotBullet spawn3 = prefab3.GetPooledInstance<BotBullet>();
                   // spawn3.transform.localPosition = spawnPos + transform.right * -1.3f;
               //     spawn3.transform.localPosition = spawnPos + botDirection * spawnDistance;
                    spawn3.transform.localPosition = spawnPos;
                    spawn3.transform.rotation = botRotation;
                    //  spawn3.transform.localPosition = transform.position + new Vector3(-1.5f, 0, 1.5f);
                    //   spawn3.transform.eulerAngles = new Vector3(transform.eulerAngles.x, transform.eulerAngles.y, transform.eulerAngles.z);
             //       spawn3.bulletID = BotID;
        


    /*    BotBullet prefab = stuffPrefabs[BotID];
        BotBullet spawn = prefab.GetPooledInstance<BotBullet>();
        spawn.transform.localPosition = spawnPos;
        spawn.transform.rotation = botRotation;
        spawn.bulletID = BotID;*/
    }
}
