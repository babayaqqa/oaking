﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
[RequireComponent(typeof(Rigidbody))]
public class Bullet : PooledObject
{

    GameObject player;
    public float speed = 5f;
    private Vector3 firstPos;
    float bulletForce;
    GameManager manager;
    [HideInInspector]
    public float forceToApply;

    [HideInInspector]
    public string whoFiredMeName;

    public int bulletID;
    GameObject WhoFiredMe;

    public float sideForce = 1f;
    float sideForcex;
    Rigidbody body;

    public float BulletDestroyDistance = 15;
    float firstBulletDestroyDistance;

    private void Awake()
    {
        player = GameObject.Find("Player");
        manager = GameObject.Find("GameManager").GetComponent<GameManager>();
        body = this.gameObject.GetComponent<Rigidbody>();
    }

    private void OnEnable()
    {
        Invoke("ReturnToPool", 2f);
    //    player = GameObject.Find("Player");
    //    manager = GameObject.Find("GameManager").GetComponent<GameManager>();
        bulletForce = player.GetComponent<Shoot>().BulletForcePlayer;

        firstPos = transform.position;
        whoFiredMeName = "player";
        WhoFiredMe = player;

        sideForcex = Random.Range(-sideForce,sideForce);
        StartCoroutine(AddForceToBullet());
        
    }
    float distBetweenBulletAndPlayer;
    private void Update()
    {

        transform.Translate(Vector3.forward * Time.deltaTime * speed);
        distBetweenBulletAndPlayer = Vector3.Distance(transform.position, player.transform.position);
      //  distBetweenBulletAndPlayer = Vector3.Distance(transform.position, firstPos);
        if (distBetweenBulletAndPlayer > BulletDestroyDistance + manager.SizeIncreaseRate * player.transform.localScale.x)
        {
            ReturnToPool();
        }
        

    }

    //this will do the samething with "setactive(false)" but I don't want to have issues. I'm doing double kill
    private void OnDisable()
    {
        CancelInvoke("ReturnToPool");
    }

     float distBetweenOtherAndPlayer;
    Vector3 forceDirection;
    private void OnTriggerEnter (Collider other)
    {
        if(other.gameObject.tag == "bot0")
        {

            distBetweenOtherAndPlayer = Vector3.Distance(player.transform.position, other.gameObject.transform.position);
            forceToApply = (1 - distBetweenOtherAndPlayer / (BulletDestroyDistance+5)) * bulletForce;
         //   other.gameObject.GetComponent<Rigidbody>().isKinematic = false;
            other.gameObject.GetComponent<Rigidbody>().AddForce(transform.forward * forceToApply);
            ReturnToPool();
            PlayerPrefs.SetString(other.GetComponent<BulletSpawner>().BotID.ToString(), "player");
        }       
    }
    private void OnTriggerExit(Collider other)
    {
        if (other.gameObject.tag == "bot0")
        {
         //   other.gameObject.GetComponent<Rigidbody>().isKinematic = true;
        }
    }

    public IEnumerator AddForceToBullet()
    {
        yield return new WaitForSeconds(0.02f);
        body.AddForce(transform.right * sideForcex);
    }

}
