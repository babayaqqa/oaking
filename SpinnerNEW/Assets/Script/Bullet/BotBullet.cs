﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
[RequireComponent(typeof(Rigidbody))]
public class BotBullet : PooledObject
{

    GameObject[] bots;
    public float speed = 5f;
    float bulletForce;
    GameManager manager;
  //  [HideInInspector]
    public int bulletID;
    public float BulletDestroyDistance = 15;
    float firstBulletDestroyDistance;
    GameObject WhoFiredMe;
    public Rigidbody Body { get; private set; }
    public float sideForce = 1f;
    float sideForcex;


    void Awake()
    {
        manager = GameObject.Find("GameManager").GetComponent<GameManager>();
        Body = GetComponent<Rigidbody>();
        bots = GameObject.FindGameObjectsWithTag("bot0");
        //   bulletForce = manager.BulletForceBot0;

    }

    bool findWhoFiredMe = false;
    private void OnEnable()
    {
        sideForcex = Random.Range(-sideForce, sideForce);
        StartCoroutine(AddForceToBullet());
        Invoke("ReturnToPool", 2f);
        
            for (int i = 0; i < bots.Length; i++)
            {
                if (bots[i] != null)
                {
                    if (bots[i].GetComponent<BulletSpawner>().BotID == bulletID)
                    {
                        WhoFiredMe = bots[i];
                        bulletForce = bots[i].GetComponent<BulletSpawner>().BulletForceBot0;
                        //   print(WhoFiredMe.GetComponent<BulletSpawner>().BotID);
                    }
                }
            }
            
    }
    private void OnDisable()
    {
        CancelInvoke("ReturnToPool");
    }


    float distBetweenBulletAndBot0;
    private void Update()
    {
        transform.Translate(Vector3.forward * Time.deltaTime * speed);
          for (int i = 0; i<bots.Length; i++)
          {
            if (bots[i] != null)
            {
                if (bots[i].GetComponent<BulletSpawner>().BotID == bulletID)
                {
                    distBetweenBulletAndBot0 = Vector3.Distance(transform.position, bots[i].transform.position);

                    if (distBetweenBulletAndBot0 > BulletDestroyDistance + manager.SizeIncreaseRate * bots[i].transform.localScale.x)
                    {
                        ReturnToPool();
                    }
                }
            }             
          }
    }

    float distBetweenOtherAndPlayer;
    float forceToApply;
    private void OnTriggerEnter (Collider other)
    {

        if (other.gameObject.tag == "player")
        {
            if (WhoFiredMe != null)
            {
                distBetweenOtherAndPlayer = Vector3.Distance(WhoFiredMe.transform.position, other.gameObject.transform.position);
                forceToApply = (1 - distBetweenOtherAndPlayer / (BulletDestroyDistance+5)) * bulletForce;
                other.gameObject.GetComponent<Rigidbody>().AddForce(transform.forward * forceToApply);
                ReturnToPool();
                PlayerPrefs.SetString("player", WhoFiredMe.GetComponent<BulletSpawner>().BotID.ToString());
            }
        }

        if (other.gameObject.tag == "bot0")
        {
            if (WhoFiredMe != null)
            {
                if (bulletID != other.GetComponent<BulletSpawner>().BotID)
                {
                    distBetweenOtherAndPlayer = Vector3.Distance(WhoFiredMe.transform.position, other.gameObject.transform.position);
                    forceToApply = (1 - distBetweenOtherAndPlayer / (BulletDestroyDistance+5)) * bulletForce;
                    other.gameObject.GetComponent<Rigidbody>().AddForce(transform.forward * forceToApply);
                    ReturnToPool();
                    PlayerPrefs.SetString(other.GetComponent<BulletSpawner>().BotID.ToString(), bulletID.ToString());
                }
            }
               
        }
         //    PlayerPrefs.SetString(other.gameObject.tag, WhoFiredMe.GetComponent<BulletSpawner>().BotID.ToString());
    }


    public IEnumerator AddForceToBullet()
    {
        yield return new WaitForSeconds(0.02f);
        this.gameObject.GetComponent<Rigidbody>().AddForce(transform.forward * sideForcex);
    }
}
