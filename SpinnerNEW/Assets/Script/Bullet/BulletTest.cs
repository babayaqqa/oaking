﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BulletTest : PooledObject
{
    public Rigidbody Body { get; private set; }
    void Awake()
    {

        Body = GetComponent<Rigidbody>();
    }
    private void OnEnable()
    {
         Invoke("ReturnToPool", 2f);

    }
    private void OnDisable()
    {
        CancelInvoke("ReturnToPool");
    }
    void OnTriggerEnter(Collider enteredCollider)
    {
        if (enteredCollider.CompareTag("player"))
        {
            ReturnToPool();
        }
    }

}
