﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using System;
public class GameManager : MonoBehaviour {
    [Header("UI")]
    public GameObject playButton;
    public GameObject replyButton;
    public GameObject titleButton;
    public GameObject nextLevelButton;
    public GameObject gameOverPanel;
    public GameObject killText;
    public GameObject GameOverKillText;
    [Header("Player")]
    public GameObject realPlayer;
    public GameObject PlayerModel;
    [Header("Bots array will be empty. prefabs will be inserted")]
    public int numberOfSpawners;
    public BulletSpawner spawnerPrefab;
    public GameObject[] bots;
    public Transform[] enemyPoints;
    public GameObject[] botModels;

    [HideInInspector]
    public int killCount;
    public Transform[] spawnPoints;
    public GameObject arena;
    public Vector3 targetScale = new Vector3(20,1,20);
    public float arenaScaleSpeed = 2;

    public float SizeIncreaseRate = 0.5f;
  //  public float BulletDestroyDistance = 15;
 //   float firstBulletDestroyDistance;
    public float additionalPowerAfterMurder = 500;
    [HideInInspector]
    public bool playing;
    private Vector3 firstScale;
    private Vector3 firstPos;
    bool dead;
    bool allbotsAreDead = false;
    float firstPower;
    int sceneIndex;
    void Start()
    {
        sceneIndex = SceneManager.GetActiveScene().buildIndex;

        firstScale.Set(100,1,100);
        killCount = 0;
        dead = false;
        killText.SetActive(false);
        playing = false;
        firstScale = arena.transform.localScale;
        firstPower = realPlayer.GetComponent<Shoot>().BulletForcePlayer;
        firstPos = realPlayer.gameObject.transform.position;
   //     firstBulletDestroyDistance = BulletDestroyDistance;

        PlayerPrefs.SetString("1", "333");
        PlayerPrefs.SetString("2", "333");
        PlayerPrefs.SetString("3", "333");
        PlayerPrefs.SetString("4", "333");
        PlayerPrefs.SetString("5", "333");
        PlayerPrefs.SetString("6", "333");
        PlayerPrefs.SetString("7", "333");
        PlayerPrefs.SetString("8", "333");
        PlayerPrefs.SetString("player", "333");

    }
    int killerID;
    void Update () {
        if (playing)
        {
            arena.transform.localScale = Vector3.Lerp(arena.transform.localScale, targetScale, arenaScaleSpeed * Time.deltaTime);
        }
        

        if (realPlayer.transform.position.y < -2.5f && !dead)
        {
            gameOver();
        }

            for (int i = 0; i < numberOfSpawners; i++)
            {
                if (bots[i] != null && bots[i].GetComponent<BulletSpawner>().dead == false)
                {
                    if (bots[i].transform.position.y < -1.5f)
                    {
                        // Destroy(bots[i]);
                        bots[i].GetComponent<BulletSpawner>().dead = true;
                    //    bots[i].GetComponent<NavMeshMovement>().enabled = false;
                    // bots[i].SetActive(false);
                    StartCoroutine(SetFalse(bots[i]));
                        if(PlayerPrefs.GetString(bots[i].GetComponent<BulletSpawner>().BotID.ToString()) == "player")
                        {
                            killCount++;
                            realPlayer.gameObject.transform.localScale += new Vector3(SizeIncreaseRate, 0, SizeIncreaseRate);
                            realPlayer.GetComponent<Shoot>().BulletForcePlayer += additionalPowerAfterMurder;
                        print("player killed the bot");
                        }
                        else
                        {
                            killerID = Convert.ToInt32(PlayerPrefs.GetString(bots[i].GetComponent<BulletSpawner>().BotID.ToString()));
                            if(killerID < bots.Length)
                            {
                                if(bots[killerID].gameObject != null)
                                {
                                    bots[killerID].gameObject.transform.localScale += new Vector3(0.5f, 0, 0.5f);
                                    bots[killerID].GetComponent<BulletSpawner>().BulletForceBot0 += additionalPowerAfterMurder;
                                }                                
                            }
                            
                        print("bot killed the bot");
                    }
                        
                      //  print("BulletForcePlayer : " + BulletForcePlayer);
                        break;
                    }
                }
            }

        killText.GetComponent<Text>().text = killCount.ToString();
        if (playing)
        {
            for (int i = 0; i < numberOfSpawners; i++)
            {
                if (bots[i].activeInHierarchy != false)
                {
                    allbotsAreDead = false;
                    break;
                }
                else if (bots[i].activeInHierarchy == false && playing)
                {
                    allbotsAreDead = true;
                }
            }
        }
           


        if (allbotsAreDead)
        {
            print("gameWIN");
            gameWin();
        }



    }

    IEnumerator SetFalse(GameObject bot)
    {
        yield return new WaitForSeconds(1f);
        bot.SetActive(false);
    }

    public void PlayGame()
    {
        realPlayer.GetComponent<Shoot>().BulletForcePlayer = firstPower;
        arena.transform.localScale = firstScale;
        realPlayer.gameObject.transform.localScale = new Vector3(2,2,2);
        realPlayer.gameObject.GetComponent<Rigidbody>().velocity = Vector3.zero;
        playing = true;
        killText.SetActive(true);
        killCount = 0;
        dead = false;
        realPlayer.SetActive(true);
        realPlayer.transform.position = firstPos;

        for (int i = 0; i < numberOfSpawners; i++)
        {
            CreateSpawner(i);
        }

        PlayerModel.GetComponent<Animator>().Play("Walk");
    }

    int modelIndex;
    void CreateSpawner(int index)
    {
        BulletSpawner spawner = Instantiate<BulletSpawner>(spawnerPrefab);        
        spawner.transform.position = spawnPoints[index].position;
        spawner.transform.localScale = new Vector3(2, 2, 2);
        spawner.BotID = index;
        bots[index] = spawner.gameObject;
        bots[index].GetComponent<BulletSpawner>().dead = false;

        modelIndex = Mathf.RoundToInt(UnityEngine.Random.Range(0,botModels.Length));
        GameObject botModel = Instantiate(botModels[modelIndex]);
        botModel.transform.SetParent(bots[index].transform);
        botModel.transform.localPosition = new Vector3(0,-0.5f,0);
        botModel.transform.localScale = new Vector3(1, 1, 1);
        botModel.GetComponent<Animator>().Play("Run");
        //	spawner.stuffMaterial = stuffMaterials[index % stuffMaterials.Length];
    }

    public void ReplyGame()
    {

        realPlayer.GetComponent<Shoot>().BulletForcePlayer = firstPower;
        arena.transform.localScale = firstScale;
        realPlayer.SetActive(true);
        realPlayer.transform.position = firstPos;
        realPlayer.gameObject.transform.localScale = new Vector3(2, 2, 2);
        realPlayer.gameObject.GetComponent<Rigidbody>().velocity = Vector3.zero;
        
        playing = true;
        killText.SetActive(true);
        killCount = 0;
        dead = false;

        for (int i = 0; i < numberOfSpawners; i++)
        {
            bots[i].SetActive(true);
            bots[i].transform.position = spawnPoints[i].transform.position;
            bots[i].gameObject.transform.localScale = new Vector3(2, 2, 2);
            bots[i].GetComponent<BulletSpawner>().dead = false;
            bots[i].gameObject.GetComponent<Rigidbody>().velocity = Vector3.zero;
            bots[i].transform.GetChild(2).GetComponent<Animator>().Play("Run");
            //     bots[i].GetComponent<NavMeshMovement>().enabled = true;
            //     bots[i].GetComponent<NavMeshMovement>().destPoint = Mathf.RoundToInt(UnityEngine.Random.Range(0, bots[i].GetComponent<NavMeshMovement>().points.Length));
        }
        PlayerModel.GetComponent<Animator>().Play("Walk");
    }


    public void gameOver()
    {
        Vibration.Vibrate(2);
        //  Vibrations.Haptic(HapticLevel.ImpactHeavy);
        playing = false;
        killText.SetActive(false);
        dead = true;
        realPlayer.GetComponent<Shoot>().EndFireTrigger();
        realPlayer.SetActive(false);
        gameOverPanel.SetActive(true);
        for (int i = 0; i < bots.Length; i++)
        {
            bots[i].SetActive(false);
        }
        GameOverKillText.GetComponent<Text>().text = killCount.ToString();
    }

    public void gameWin()
    {
        playing = false;
        realPlayer.GetComponent<Shoot>().EndFireTrigger();
        killText.SetActive(false);
        allbotsAreDead = false;
        gameOverPanel.SetActive(true);
        GameOverKillText.GetComponent<Text>().text = killCount.ToString();
        nextLevelButton.SetActive(true);
    }

    public void LoadNextLevel()
    {
        SceneManager.LoadScene(sceneIndex+1);
    }


}
